MSG *** Ring topology ***
MSG Ring number:   00 01 02 03 04 05 06 07 08 09 10 11 
MSG Number of slaves: 00 00 02 00 00 00 00 00 00 00 00 00 
MSG Bulk Data Packet size : 7
MSG Check BE has emerged from reset
AGET %s-%s:RING-REGS-MST-SRST-RBV 0
MSG Power-down all BE cplls.
SET %s-%s:RING-REGS-MST-PDPL-SP 4294967295
AGET %s-%s:RING-REGS-MST-PDPL-RBV 4294967295
MSG Power up the required BE cplls
SET %s-%s:RING-REGS-MST-PDPL-SP 4294967247
AGET %s-%s:RING-REGS-MST-PDPL-RBV 4294967247
MSG Reset all BE GTYs.
SET %s-%s:RING-REGS-MST-RGTY-SP 4294967295
AGET %s-%s:RING-REGS-MST-RGTY-RBV 4294967295
MSG Power up the required BE GTYs.
SET %s-%s:RING-REGS-MST-RGTY-SP 4294967247
AGET %s-%s:RING-REGS-MST-RGTY-RBV 4294967247
MSG Check the BE reset controller sees the requisite GTYs out of reset.
AGET %s-%s:RING-REGS-MST-RTXD-RBV 48
AGET %s-%s:RING-REGS-MST-RRXD-RBV 48
MSG Put all BE tx- and rx- engines into their IDLE state.
SET %s-%s:RING-REGS-MST-TNS-00-SP 0
SET %s-%s:RING-REGS-MST-TNS-01-SP 0
SET %s-%s:RING-REGS-MST-TNS-02-SP 0
SET %s-%s:RING-REGS-MST-RNS-00-SP 0
SET %s-%s:RING-REGS-MST-RNS-01-SP 0
SET %s-%s:RING-REGS-MST-RNS-02-SP 0
MSG Set up the length of bulk data packet the BE will later egress
SET %s-%s:RING-REGS-MST-PLEN-SP 7
MSG Set the BE TDIR register to dictate whether Even(TDIR=0x0) or Odd(TDIR=0x1) xcvr's will be used for bring-up
SET %s-%s:RING-REGS-MST-TDIR-SP 0
SET %s-%s:RING-REGS-MST-BRINGUP-STATUS-02-SP 66048
MSG Instruct BE TX xcvr 04 to transmit 95959BC synchronisation sequence.
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 65536 4293984255
MSG Instruct BE RX xcvr 04 to listen for 95959BC return sequence.
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 65536 4293984255
MSG Poll the snapshot register of BE tx-engine 04 to confirm xcvr is egressing 959595BC.
PGET %s-%s:RING-REGS-MST-TS-04-RBV 2509608380
MSG Poll bit 04 of the BE LCKD register to confirm lock to the 959595BC return sequence.
MSG The data being received in RS04 will be reported after every 7 polling attempts.
PLOCK %s-%s:RING-REGS-MST-RS-04-RBV 16 4294967279
MSG Enabling slow control for Slave 0 with the BE xcvr 04
GET %s-%s:RING-REGS-MST-TNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 131072 4293984255
GET %s-%s:RING-REGS-MST-RNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 131072 4293984255
MSG Check the BE has slow-control over Slave 31 by reading its VERS register
GET %s-%s:RING-REGS-SLV-VERS-SLV-R02-N31-RBV
MSG The slave CSEL register reports which of the sub-ring's the slave has selected for clock recovery.
MSG Reading is not essential for bring up, but may aid in future remote-determination of SFP-cable topology and/or timestamp analyses.
GET %s-%s:RING-REGS-SLV-CSEL-SLV-R02-N31-RBV
MSG Re-name Slave node_id from 31 to 0
SET %s-%s:RING-REGS-SLV-NDID-SLV-R02-N31-SP 0
MSG Check the BE has slow-control over Slave 0 by reading its VERS register
GET %s-%s:RING-REGS-SLV-VERS-SLV-R02-N00-RBV
MSG Setting up the time on Slave 0
MSG To reduce the number of arithemtic operations done by Slaves when creating strobes, the LCL_1Hz_TAR register
MSG should be set to the timing-clk-freq-minus-one e.g. 88052500-1 = 0x053f9313.
SET %s-%s:RING-REGS-SLV-LCL-1HZ-TAR-SLV-R02-N00-SP 88052499
MSG Instructing BE TX-engine 04 to enter time-setting mode
SET %s-%s:RING-REGS-MST-TMMD-SP 16
MSG When writing a Slave's (STME) time register, the BE timestamps the outgoing 
MSG command and slave's response in the BE SETTSNTxx and SETTGOTxx registers.
MSG The difference (RM-TM) indicates a time-of-flight for the packet.
MSG Once known, the TOF is stored in the BE TMOF register, providing an offset for a second slave time-set.
MSG WARNING : The following invokes a call to a realtime (python) software TOF calculation routine. 
MSG This has no meaning in VHDL testbench mode and TOFs are instead assigned an arbitary value TESTBENCH_TOF x position-on-ring
MSG  
MSG Perform a first (dummy) STME_SLV to record timestamps into the BE's TM and RM registers.
SET %s-%s:RING-REGS-SLV-STME-SLV-R02-N00-SP 2882343476
MSG The first STME_SLV command received by a Slave results in a measurement of the local delay through the FE's ring-inteconnect.
MSG This measured delay is stored in the ICDY_SLVxx register. Read it next for use in subsequent TOF calculation.
SET %s-%s:RING-REGS-MST-TMMD-SP 0
GET %s-%s:RING-REGS-SLV-ICDY-SLV-R02-N00-RBV
SET %s-%s:RING-REGS-MST-TMMD-SP 16
SW_TOF_CALC %s-%s:RING-REGS-MST-SETTSNT-04-RBV %s-%s:RING-REGS-MST-SETTGOT-04-RBV
MSG With the TOF and Slaves' inteconnect delay now known and poked into the BE TMOF register by software, perform a second STME_SLV.
SET %s-%s:RING-REGS-SLV-STME-SLV-R02-N00-SP 2882343476
MSG Instructing BE TX-engine to leave time-setting mode
SET %s-%s:RING-REGS-MST-TMMD-SP 0
SET %s-%s:RING-REGS-MST-BRINGUP-STATUS-02-SP 131584
MSG Setup of Slave 0 complete. Reverse the its interconnect to forward traffic to the next slave.
SET %s-%s:RING-REGS-SLV-RVSR-SLV-R02-N00-SP 0
SET %s-%s:RING-REGS-MST-BRINGUP-STATUS-02-SP 66049
MSG Instruct BE TX xcvr 04 to transmit 95959BC synchronisation sequence.
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 65536 4293984255
MSG Instruct BE RX xcvr 04 to listen for 95959BC return sequence.
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 65536 4293984255
MSG Poll the snapshot register of BE tx-engine 04 to confirm xcvr is egressing 959595BC.
PGET %s-%s:RING-REGS-MST-TS-04-RBV 2509608380
MSG Poll bit 04 of the BE LCKD register to confirm lock to the 959595BC return sequence.
MSG The data being received in RS04 will be reported after every 7 polling attempts.
PLOCK %s-%s:RING-REGS-MST-RS-04-RBV 16 4294967279
MSG Enabling slow control for Slave 1 with the BE xcvr 04
GET %s-%s:RING-REGS-MST-TNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 131072 4293984255
GET %s-%s:RING-REGS-MST-RNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 131072 4293984255
MSG Check the BE has slow-control over Slave 31 by reading its VERS register
GET %s-%s:RING-REGS-SLV-VERS-SLV-R02-N31-RBV
MSG The slave CSEL register reports which of the sub-ring's the slave has selected for clock recovery.
MSG Reading is not essential for bring up, but may aid in future remote-determination of SFP-cable topology and/or timestamp analyses.
GET %s-%s:RING-REGS-SLV-CSEL-SLV-R02-N31-RBV
MSG Re-name Slave node_id from 31 to 1
SET %s-%s:RING-REGS-SLV-NDID-SLV-R02-N31-SP 1
MSG Check the BE has slow-control over Slave 1 by reading its VERS register
GET %s-%s:RING-REGS-SLV-VERS-SLV-R02-N01-RBV
MSG Setting up the time on Slave 1
MSG To reduce the number of arithemtic operations done by Slaves when creating strobes, the LCL_1Hz_TAR register
MSG should be set to the timing-clk-freq-minus-one e.g. 88052500-1 = 0x053f9313.
SET %s-%s:RING-REGS-SLV-LCL-N01HZ-TAR-SLV-R02-N01-SP 88052499
MSG Instructing BE TX-engine 04 to enter time-setting mode
SET %s-%s:RING-REGS-MST-TMMD-SP 16
MSG When writing a Slave's (STME) time register, the BE timestamps the outgoing 
MSG command and slave's response in the BE SETTSNTxx and SETTGOTxx registers.
MSG The difference (RM-TM) indicates a time-of-flight for the packet.
MSG Once known, the TOF is stored in the BE TMOF register, providing an offset for a second slave time-set.
MSG WARNING : The following invokes a call to a realtime (python) software TOF calculation routine. 
MSG This has no meaning in VHDL testbench mode and TOFs are instead assigned an arbitary value TESTBENCH_TOF x position-on-ring
MSG  
MSG Perform a first (dummy) STME_SLV to record timestamps into the BE's TM and RM registers.
SET %s-%s:RING-REGS-SLV-STME-SLV-R02-N01-SP 2882343476
MSG The first STME_SLV command received by a Slave results in a measurement of the local delay through the FE's ring-inteconnect.
MSG This measured delay is stored in the ICDY_SLVxx register. Read it next for use in subsequent TOF calculation.
SET %s-%s:RING-REGS-MST-TMMD-SP 0
GET %s-%s:RING-REGS-SLV-ICDY-SLV-R02-N01-RBV
SET %s-%s:RING-REGS-MST-TMMD-SP 16
SW_TOF_CALC %s-%s:RING-REGS-MST-SETTSNT-04-RBV %s-%s:RING-REGS-MST-SETTGOT-04-RBV
MSG With the TOF and Slaves' inteconnect delay now known and poked into the BE TMOF register by software, perform a second STME_SLV.
SET %s-%s:RING-REGS-SLV-STME-SLV-R02-N01-SP 2882343476
MSG Instructing BE TX-engine to leave time-setting mode
SET %s-%s:RING-REGS-MST-TMMD-SP 0
SET %s-%s:RING-REGS-MST-BRINGUP-STATUS-02-SP 131585
MSG Closing the ring on final slave(=1) of Ring 2
MSG Instruct BE TX xcvr 05 to transmit 95959BC synchronisation sequence.
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 1048576 4279238655
MSG Instruct BE RX xcvr 05 to listen for 95959BC return sequence.
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 1048576 4279238655
MSG Poll the snapshot register of BE tx-engine 05 to confirm xcvr is egressing 959595BC.
PGET %s-%s:RING-REGS-MST-TS-05-RBV 2509608380
MSG Poll bit 05 of the BE LCKD register to confirm lock to the 959595BC return sequence.
MSG The data being received in RS05 will be reported after every 7 polling attempts.
PLOCK %s-%s:RING-REGS-MST-RS-05-RBV 32 4294967263
MSG Enabling slow control for Slave 1 with the BE xcvr 05
GET %s-%s:RING-REGS-MST-TNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 2097152 4279238655
GET %s-%s:RING-REGS-MST-RNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 2097152 4279238655
MSG Setup of Slave 1 complete. Reverse the its interconnect to forward traffic to the next slave.
SET %s-%s:RING-REGS-SLV-RVSR-SLV-R02-N01-SP 0
MSG Ring 2 is now closed. Start 95BC on both rings and perform a final reset of both BE Rx-GTYs
MSG Instruct BE TX xcvr 04 to transmit 95959BC synchronisation sequence.
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 65536 4293984255
MSG Instruct BE RX xcvr 04 to listen for 95959BC return sequence.
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 65536 4293984255
MSG Instruct BE TX xcvr 05 to transmit 95959BC synchronisation sequence.
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 1048576 4279238655
MSG Instruct BE RX xcvr 05 to listen for 95959BC return sequence.
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 1048576 4279238655
MSG Poll the snapshot register of BE tx-engine 04 to confirm xcvr is egressing 959595BC.
PGET %s-%s:RING-REGS-MST-TS-04-RBV 2509608380
MSG Poll bit 04 of the BE LCKD register to confirm lock to the 959595BC return sequence.
MSG The data being received in RS04 will be reported after every 7 polling attempts.
PLOCK %s-%s:RING-REGS-MST-RS-04-RBV 16 4294967279
MSG Poll the snapshot register of BE tx-engine 05 to confirm xcvr is egressing 959595BC.
PGET %s-%s:RING-REGS-MST-TS-05-RBV 2509608380
MSG Poll bit 05 of the BE LCKD register to confirm lock to the 959595BC return sequence.
MSG The data being received in RS05 will be reported after every 7 polling attempts.
PLOCK %s-%s:RING-REGS-MST-RS-05-RBV 32 4294967263
MSG Both rings fully locked. Move both to slow control.
MSG Enabling Slow Control for RX engines 04 and 05 in ring 2
GET %s-%s:RING-REGS-MST-RNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 131072 4293984255
GET %s-%s:RING-REGS-MST-RNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-RNS-00-SP 2097152 4279238655
MSG Enabling Slow Control for TX engines 04 and 05 in ring 2
GET %s-%s:RING-REGS-MST-TNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 131072 4293984255
GET %s-%s:RING-REGS-MST-TNS-00-RBV
MSK_SET %s-%s:RING-REGS-MST-TNS-00-SP 2097152 4279238655
SET %s-%s:RING-REGS-MST-BRINGUP-STATUS-02-SP 16908801
MSG Configure the BE MNxx register to launch packets for all slaves, round-robin-style
SET %s-%s:RING-REGS-MST-MN-04-SP 256
MSG Configure the BE MNxx register to launch packets for all slaves, round-robin-style
SET %s-%s:RING-REGS-MST-MN-05-SP 256
MSG !!!!!!!!!!!!!!!! Ring 2 setup is complete - moving to checks !!!!!!!!!!!!!!
MSG Sanity check all slave timestamps
MSG Slave times on ring 2
GET %s-%s:RING-REGS-SLV-TM-INT-SLV-R02-N00-RBV
GET %s-%s:RING-REGS-SLV-TM-FRAC-SLV-R02-N00-RBV
GET %s-%s:RING-REGS-SLV-TM-INT-SLV-R02-N01-RBV
GET %s-%s:RING-REGS-SLV-TM-FRAC-SLV-R02-N01-RBV
MSG Sanity checks of timestamps on 2 complete. 
