

require detector_readout 
require stream
require autosave,5.10.2
require seq

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "ring-dev-01")
epicsEnvSet("SYS", "RNG-DEV")
epicsEnvSet("DEV", "01")
epicsEnvSet("EPICS_CMDS", "db")
epicsEnvSet("EVR", "EVR-99")
epicsEnvSet("RING_FILE", "ring_files/ring_check_epics.txt")

############# -------- Detector Readout Interface ----------------- ##################
epicsEnvSet("STREAM_PROTOCOL_PATH","$(detector_readout_DIR)/db")
epicsEnvSet("PROTO", "ics-dg-udp.proto")
epicsEnvSet("DestIP", "192.168.50.2")
epicsEnvSet("DestPort", "65535")
epicsEnvSet("SrcPort", "65534")
drvAsynIPPortConfigure ("UDP","$(DestIP):$(DestPort):$(SrcPort) UDP")

#var streamDebug 1
#asynSetTraceMask("UDP",-1,0x9) 
#asynSetTraceIOMask("UDP",-1,0x4)

dbLoadRecords("detector_ring.db", "SYS=$(SYS), DEV=$(DEV), COM=UDP, REG0=-1073737728, SCAN=Passive, SPACE=RING-REGS-MST, PRO=$(PROTO), EVR=$(EVR)")
dbLoadRecords("timing_mode.db", "SYS=$(SYS), DEV=$(DEV), COM=UDP, REG0=-1073737728, SCAN=Passive, SPACE=CTL-MST, PRO=$(PROTO), EVR=$(EVR)")
dbLoadRecords("i2c_sensors.db", "SYS=$(SYS), DEV=$(DEV), COM=UDP, SCAN=Passive, SPACE=CTL-REGS-MST")

iocshLoad("$(EPICS_CMDS)/cmac_regs_mst.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("$(EPICS_CMDS)/ctl_regs_mst.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("$(EPICS_CMDS)/eng_regs_mst.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("$(EPICS_CMDS)/ring_regs_mst.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("$(EPICS_CMDS)/time_regs_mst.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")
iocshLoad("$(EPICS_CMDS)/ring_regs_slv.cmd", "DEV=$(DEV), COM=UDP, SYS=$(SYS), PROTO=$(PROTO)")

iocInit()

epicsThreadSleep(5)

dbl > pv.list
dbpf $(SYS)-$(DEV):FilePath $(RING_FILE)
dbpf  $(SYS)-$(DEV):EnableDebugSNL 1

dbpf $(SYS)-$(DEV):CTL-REGS-MST-TEMP0-RBV.SCAN 6
dbpf $(SYS)-$(DEV):CTL-REGS-MST-TEMP1-RBV.SCAN 6
dbpf $(SYS)-$(DEV):CTL-REGS-MST-TEMP2-RBV.SCAN 6
dbpf $(SYS)-$(DEV):CTL-REGS-MST-TEMP-HIGH-RBV.SCAN 6

dbpf $(SYS)-$(DEV):CTL-REGS-MST-DEVICE-DNA-RD.PROC 1
dbpf $(SYS)-$(DEV):CTL-REGS-MST-GIT-HASH-RD.PROC 1

dbpf $(SYS)-$(DEV):#RefFrequencyCalc.PROC 1
dbpf $(SYS)-$(DEV):#TimingFrequencyCalc.PROC 1

dbpf $(SYS)-$(DEV):TIME-REGS-MST-CMN-OUT-TS-INT-RBV.SCAN 6

dbpf $(SYS)-$(DEV):MRFStatus-RB.PROC 1

seq ring_bringup "SYS=$(SYS), DEV=$(DEV)"
